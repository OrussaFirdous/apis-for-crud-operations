const express = require('express');
const app = express();
const PORT = 5000;
const fs = require('fs');
var bodyparser = require('body-parser');

app.use(bodyparser.urlencoded({extended: false}));

let family_tree = [
	{ 
	  "id":1,
	  "firstname" : "Hasibur",
	  "lastname" : "Rahman Khan",
	  "age" : 85,
	  "relation" : "Dada"}

];

app.get('/',(req,res) => {
	res.json("Server Started!");
	}
);


app.get('/all', (req,res) => {		
		res.json(family_tree);
	}
);

app.get('/:id', (req,res) => {

	let req_id = parseInt(req.params.id);

	if(family_tree.length > 0 && family_tree[0] != null){
			let obj_toBe_Retreived = getId(req_id);

			if(obj_toBe_Retreived === 0 || obj_toBe_Retreived){
				res.json(family_tree[obj_toBe_Retreived]);
			}
			else
				return res.status(404).json({ error : 'Bad Request'});
	}
	else{
		return res.status(404).json({ error: 'Bad Request' });
	}

});


app.post('/add', (req, res) => {

	const{id,firstname,lastname,age,relation} = req.body;
	let oldMember;


	if(family_tree.length > 0 && family_tree[0] != null){
	 	oldMember =  family_tree.find( f => f.id === parseInt(id) &&
		f.firstname === firstname && f.lastname === lastname && f.age === parseInt(age));
	}

	if(oldMember){
		 return res.status(400).json({ error: 'Member Already added to the Family' });
	}
		
		let new_member = {};
		new_member["id"]=parseInt(id);
		new_member["firstname"]=firstname;
		new_member["lastname"]=lastname;
		new_member["age"]=parseInt(age);
		new_member["relation"]=relation;
		family_tree.push(new_member);
		console.log(new_member);
	  	res.json("Member added Successfully");

});





app.put('/member/:id' , (req,res) => {

		let  req_id = parseInt(req.params.id);
		let updated_object = req.body;
		let elem = getId(req_id);
		
		

				if(family_tree.find( f => f.id === req_id)){
					family_tree.splice(elem,1,updated_object);
					res.json("Updated your record Successfully!!")
				}
				else{
						return res.status(404).json({ error: 'Bad Request' });
				}

});


app.delete('/remove/:id' , (req,res) => {

		let req_id = parseInt(req.params.id);
		

		if(family_tree.length > 0 && family_tree[0] != null){

			let obj_toBe_Deleted = getId(req_id);
				
				if(family_tree.find( f => f.id === req_id)){
					delete family_tree[obj_toBe_Deleted];
					res.json("DELETE REQUEST FULFILLED!")
				}
				else{
					console.log(obj_toBe_Deleted);
					return res.status(404).json({ error: 'Bad Request' });
				}

		}
		else{
			return res.status(502).json({ error: 'Bad Gateway' });
		}

	}
);

let getId = function(j){
	for(let i=0;i<family_tree.length;i++){
			if(family_tree[i].id === j)
				return i;
	}
	return undefined;
}
/*
function readFileHERE(){
			try {
						const databases = require('./members.json');

							// print all databases
							databases.forEach(db => {
							    console.log(`${db.name}: ${db.type}`);
							});

			} catch (err) {
			    console.log(`Error reading file from disk: ${err}`);
			}
}

function writeFile(arr){
    const familyData = JSON.stringify(arr);     
    fs.writeFileSync(__dirname + "/members.json", familyData); 
}
*/
app.listen(PORT, () => console.log('http://localhost: ' + PORT));
